# REST API
> 2021-12-17 César Freire


## deploy

activate profile linux  
`$ export spring_profiles_active=<profile>`

activate profile windows  
`$ set spring_profiles_active=<profile>`

local run  
`$ ./mvnw spring-boot:run`

local run with profile  
`$ ./mvnw spring-boot:run -Dspring-boot.run.profiles=development`

run final solution  
`$ java -jar target/rest_api-<version>.jar --spring.profiles.active=development`

---

## build

clean target  
`$ ./mvnw clean`

target build  
`$ ./mvnw package`

docker build
`$ git clone ${url}`
`$ cd spring-rest-api`
`$ docker build -t spring-rest-api:latest .`

sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "GFsQddmzinWiQyBnKp_b" \
  --executor "shell" \
  --description "docker-lab-node2" \
  --tag-list "develop_server_tag" \
  --run-untagged="false" \
  --locked="false" \
  --access-level="not_protected"

sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "GFsQddmzinWiQyBnKp_b" \
  --executor "shell" \
  --description "docker-lab-node1" \
  --tag-list "production_server_tag" \
  --run-untagged="false" \
  --locked="false" \
  --access-level="not_protected"

sudo gitlab-runner register \
--non-interactive \
--url "https://gitlab.com/" \
--registration-token "GFsQddmzinWiQyBnKp_b" \
--executor "docker" \
--description "docker-lab-node2" \
--tag-list "develop_server_docker" \
--docker-image "maven:3-openjdk-17-slim" \
--run-untagged="false" \
--locked="false" \
--access-level="not_protected"